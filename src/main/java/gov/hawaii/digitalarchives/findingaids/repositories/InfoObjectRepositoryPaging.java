package gov.hawaii.digitalarchives.findingaids.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import gov.hawaii.digitalarchives.findingaids.entities.InfoObjectEntity;

@Repository
public interface InfoObjectRepositoryPaging extends PagingAndSortingRepository<InfoObjectEntity, Long> {

    Page<InfoObjectEntity> findByLevelOrderByTitleAsc(Pageable pageable, String level);

    Page<InfoObjectEntity> findByLevelOrderByTitleDesc(Pageable pageable, String level);

    Page<InfoObjectEntity> findByLevel(Pageable pageable, String level);
}
