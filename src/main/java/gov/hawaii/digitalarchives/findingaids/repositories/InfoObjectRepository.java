package gov.hawaii.digitalarchives.findingaids.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import gov.hawaii.digitalarchives.findingaids.entities.InfoObjectEntity;

@Repository
public interface InfoObjectRepository extends CrudRepository<InfoObjectEntity, Long> {

    InfoObjectEntity findByParentId(Long id);

    List<InfoObjectEntity> findByLevel(String level);

    List<InfoObjectEntity> findByLevelOrderByTitleAsc(String level);

    List<InfoObjectEntity> findByLevelOrderByTitleDesc(String level);

}