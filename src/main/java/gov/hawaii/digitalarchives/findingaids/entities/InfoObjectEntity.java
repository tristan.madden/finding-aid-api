package gov.hawaii.digitalarchives.findingaids.entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tbl_nodes")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class InfoObjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String access;
    @Lob
    private String adminHistory;
    private String arrangement;
    private String code;
    private String creator;
    private Date dateBegin;
    private Date dateEnd;
    @Lob
    private String extentDescription;
    private String extentSize;
    private String formerReference;
    private String heldBy;
    private String language;
    private String level;
    @Lob
    private String notes;
    @Column(name = "parent_id", insertable = false, updatable = false)
    private Long parentId;
    private String reference;
    private String relatedMaterial;
    @Lob
    private String scopeContent;
    private String title;

    // Parent Relationship
    @ManyToOne(optional = true)
    private InfoObjectEntity parent;

    @JsonIgnore
    public InfoObjectEntity getParent() {
        return parent;
    }

    public void setParent(InfoObjectEntity parent) {
        this.parent = parent;
    }

    // Child Relationship
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InfoObjectEntity> children;

    @JsonIgnore
    public List<InfoObjectEntity> getChildren() {
        return children;
    }

    public void setChildren(List<InfoObjectEntity> children) {
        this.children = children;
    }

    // Getters and Setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getAdminHistory() {
        return adminHistory;
    }

    public void setAdminHistory(String adminHistory) {
        this.adminHistory = adminHistory;
    }

    public String getArrangement() {
        return arrangement;
    }

    public void setArrangement(String arrangement) {
        this.arrangement = arrangement;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getExtentDescription() {
        return extentDescription;
    }

    public void setExtentDescription(String extentDescription) {
        this.extentDescription = extentDescription;
    }

    public String getExtentSize() {
        return extentSize;
    }

    public void setExtentSize(String extentSize) {
        this.extentSize = extentSize;
    }

    public String getFormer_reference() {
        return formerReference;
    }

    public void setFormer_reference(String former_reference) {
        this.formerReference = former_reference;
    }

    public String getHeldBy() {
        return heldBy;
    }

    public void setHeldBy(String heldBy) {
        this.heldBy = heldBy;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRelated_material() {
        return relatedMaterial;
    }

    public void setRelated_material(String related_material) {
        this.relatedMaterial = related_material;
    }

    public String getScopeContent() {
        return scopeContent;
    }

    public void setScopeContent(String scopeContent) {
        this.scopeContent = scopeContent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "InfoObjectEntity{" + "id=" + id + ", access=" + access + ", adminHistory=" + adminHistory
                + ", arrangement=" + arrangement + ", code=" + code + ", creator=" + creator + ", dateBegin="
                + dateBegin + ", dateEnd=" + dateEnd + ", extentDescription=" + extentDescription + ", extentSize="
                + extentSize + ", formerReference=" + formerReference + ", heldBy=" + heldBy + ", language=" + language
                + ", level=" + level + ", notes=" + notes + ", parentId=" + parentId + ", reference=" + reference
                + ", relatedMaterial=" + relatedMaterial + ", scopeContent=" + scopeContent + ", title=" + title
                + ", parent=" + parent + ", children=" + children + '}';
    }

}
