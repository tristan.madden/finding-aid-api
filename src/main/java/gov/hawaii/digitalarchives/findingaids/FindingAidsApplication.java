package gov.hawaii.digitalarchives.findingaids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindingAidsApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(FindingAidsApplication.class, args);
	}
}
