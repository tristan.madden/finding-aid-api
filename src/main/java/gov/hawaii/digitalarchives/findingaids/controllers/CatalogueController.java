//Documentation:
//https://documenter.getpostman.com/view/5450839/RWaRMQLi

package gov.hawaii.digitalarchives.findingaids.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import gov.hawaii.digitalarchives.findingaids.entities.InfoObjectEntity;
import gov.hawaii.digitalarchives.findingaids.repositories.InfoObjectRepository;

@CrossOrigin
@RestController
@RequestMapping(path = "/catalogue")
public class CatalogueController {
    @Autowired
    private InfoObjectRepository repo;
    @Autowired
    private CataloguePagingService service;

    /***************************
     * D E L E T E M A P P I N G
     ***************************/

    // WORKS IN POSTMAN
    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody InfoObjectEntity infoObject) {
        Long id = infoObject.getId();
        InfoObjectEntity dead = repo.findById(id).orElseThrow(IllegalStateException::new);
        repo.delete(dead);
        return new ResponseEntity<>(dead, HttpStatus.ACCEPTED);
    }

    // Working example(s):
    // http://localhost:8080/catalogue/delete/38
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
        Optional<InfoObjectEntity> dead = repo.findById(id);
        repo.delete(dead.get());
        return new ResponseEntity<>(dead, HttpStatus.ACCEPTED);
    }

    /*********************
     * G E T M A P P I N G
     *********************/

    // Working example(s):
    // http://localhost:8080/catalogue/get/page?page=1&size=10&direction=desc&sort=title
    // http://localhost:8080/catalogue/get/page?page=1&size=10&direction=desc&sort=title&level=department
    @GetMapping("/get/page")
    public List<InfoObjectEntity> pageAtLevel(@RequestParam("page") int page, @RequestParam("size") int size,
            @RequestParam("direction") String direction, @RequestParam("sort") String sort,
            @RequestParam(value = "level", required = false) String level) {
        if (level == null) {
            return service.findPage(page, size, direction, sort);
        } else {
            return service.findPageByLevel(page, size, direction, sort, level);
        }
    }

    @GetMapping("/get/page/count")
    public long pageCount(@RequestParam("size") int size, @RequestParam("level") String level) {
        return service.getPageCountAtLevel(size, level);
    }

    // Working example(s):
    // http://localhost:8080/catalogue/get/everything
    @GetMapping("/get/everything")
    public List<InfoObjectEntity> getEverything() {
        List<InfoObjectEntity> query = new ArrayList<InfoObjectEntity>();
        repo.findAll().forEach(query::add);
        return query;

    }

    // Working example(s):
    // http://localhost:8080/catalogue/get/56
    @GetMapping("/get/{id}")
    public Optional<InfoObjectEntity> getId(@PathVariable("id") Long id) {
        Optional<InfoObjectEntity> query = repo.findById(id);

        return query;
    }

    // Working example(s):
    // http://localhost:8080/catalogue/get/parent/31
    @GetMapping("/get/parent/{id}")
    public Optional<InfoObjectEntity> getParentUsingId(@PathVariable("id") Long id) {
        Optional<InfoObjectEntity> child = repo.findById(id);
        Long pid = child.get().getParentId();
        System.out.println(pid);
        Optional<InfoObjectEntity> parent = Optional.of(child.get().getParent());
        return parent;
    }

    // Working example(s):
    // http://localhost:8080/catalogue/get/child/39
    @GetMapping("/get/children/{id}")
    public List<InfoObjectEntity> getChildrenUsingId(@PathVariable("id") Long id) {
        Optional<InfoObjectEntity> parent = repo.findById(id);
        List<InfoObjectEntity> children = parent.get().getChildren();
        return children;
    }

    // Working example(s):
    // http://localhost:8080/catalogue/get/level/division?sort
    // http://localhost:8080/catalogue/get/level/division?sort=asc
    // http://localhost:8080/catalogue/get/level/division?sort=desc
    @GetMapping("/get/level/{depth}{sort}")
    public List<InfoObjectEntity> getLevelAsc(@PathVariable(value = "depth", required = true) String depth,
            @RequestParam(value = "sort", required = false) String sort) {

        List<InfoObjectEntity> query;

        if (sort.toLowerCase().contains("asc")) {
            query = repo.findByLevelOrderByTitleAsc(depth);
        } else if (sort.toLowerCase().contains("desc")) {
            query = repo.findByLevelOrderByTitleDesc(depth);
        } else {
            query = repo.findByLevel(depth);
        }

        return query;
    }

    /***********************
     * P O S T M A P P I N G
     ***********************/

    // WORKS IN POSTMAN
    @PostMapping(value = "/post")
    public ResponseEntity<?> postIt(@RequestBody PostWrapper postWrapper) {
        InfoObjectEntity temp = postWrapper.getChild();
        temp.setParent(postWrapper.parent);
        repo.save(temp);
        return new ResponseEntity<>(temp, HttpStatus.ACCEPTED);
    }

    /*********************
     * P U T M A P P I N G
     *********************/

    // WORKS IN POSTMAN
    @PutMapping(value = "/put")
    public ResponseEntity<Object> putItem(@RequestBody InfoObjectEntity infoObject) {

        Optional<InfoObjectEntity> oldObj = repo.findById(infoObject.getId());
        InfoObjectEntity newObj = infoObject;
        
        //Not sure if this is actually necessary
        newObj.setParent(oldObj.get().getParent());
        newObj.setChildren(oldObj.get().getChildren());

        repo.save(newObj);
        return new ResponseEntity<>(newObj, HttpStatus.ACCEPTED);
    }

}