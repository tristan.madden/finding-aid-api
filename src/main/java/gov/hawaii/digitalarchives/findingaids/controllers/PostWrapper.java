package gov.hawaii.digitalarchives.findingaids.controllers;

import org.springframework.lang.Nullable;

import gov.hawaii.digitalarchives.findingaids.entities.InfoObjectEntity;

public class PostWrapper {

    InfoObjectEntity child;
    @Nullable
    InfoObjectEntity parent;

    public PostWrapper(InfoObjectEntity child, InfoObjectEntity parent) {
        this.child = child;
        this.parent = parent;
    }

    public InfoObjectEntity getChild() {
        return this.child;
    }

    public InfoObjectEntity getParent() {
        return this.parent;
    }

}
