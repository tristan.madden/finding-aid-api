package gov.hawaii.digitalarchives.findingaids.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import gov.hawaii.digitalarchives.findingaids.entities.InfoObjectEntity;
import gov.hawaii.digitalarchives.findingaids.repositories.InfoObjectRepositoryPaging;

@Service
public class CataloguePagingService {

    @Autowired
    private InfoObjectRepositoryPaging pagingRepo;

    public List<InfoObjectEntity> findPage(int page, int size, String direction, String sort) {
        List<InfoObjectEntity> queryList = new ArrayList<InfoObjectEntity>();
        if (page <= 0) {
            page = 1;
        }
        Sort.Direction sortDir = Sort.Direction.ASC;
        if (direction.contains("asc") || direction.contains("ASC")) {
        } else if (direction.contains("desc") || direction.contains("DESC")) {
            sortDir = Sort.Direction.DESC;
        }
        Page<InfoObjectEntity> pages = pagingRepo.findAll(PageRequest.of(--page, size, sortDir, sort));
        for (InfoObjectEntity p : pages) {
            queryList.add(p);
        }
        return queryList;
    }

    public List<InfoObjectEntity> findPageByLevel(int page, int size, String direction, String sort, String level) {
        List<InfoObjectEntity> queryList = new ArrayList<InfoObjectEntity>();
        if (page <= 0) {
            page = 1;
        }
        Sort.Direction sortDir = Sort.Direction.ASC;
        if (direction.contains("asc") || direction.contains("ASC")) {
        } else if (direction.contains("desc") || direction.contains("DESC")) {
            sortDir = Sort.Direction.DESC;
        }
        Page<InfoObjectEntity> pages = pagingRepo
                .findByLevelOrderByTitleAsc(PageRequest.of(--page, size, sortDir, sort), level);
        for (InfoObjectEntity p : pages) {
            queryList.add(p);
        }
        return queryList;
    }

    public long getPageCountAtLevel(int size, String level) {
        Page<InfoObjectEntity> pages = pagingRepo.findByLevel(PageRequest.of(1, size), level);
        return pages.getTotalPages();
    }

}